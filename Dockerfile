FROM jenkinsci/blueocean:latest
USER root
COPY plugins.txt /var/jenkins_home/plugins.txt
RUN mkdir -p /var/maven_home
COPY apache-maven-3.5.4-bin.zip /var/maven_home/apache-maven-3.5.4-bin.zip
RUN unzip /var/maven_home/apache-maven-3.5.4-bin.zip -d /var/maven_home
RUN /usr/local/bin/plugins.sh /var/jenkins_home/plugins.txt

COPY config.xml /usr/share/jenkins/ref/jobs/DSL_Seed/config.xml
COPY DSL_seed.groovy /usr/share/jenkins/ref/jobs/DSL_Seed/workspace/DSL_seed.groovy
