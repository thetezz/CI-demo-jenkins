pipelineJob('Snapshot_Pipeline') {
  logRotator(-1, 10, -1, -1)
  
    parameters{
    choiceParam('PRODUCT_NAME', ['database-service', 'cas-service', 'StormProject'],
               "artifact ID of the RPM to build")
    stringParam("BRANCH_NAME", "development", "branch to build")
  }

  definition {
        cpsScm {
            scm {
                git {
                    remote {
                       url('https://gitlab.com/thetezz/snapshot-pipeline.git')
                    }
                    branch('*/master')
                    extensions{
                      wipeOutWorkspace()
                    }
                  
                }
            }
        }
    }
}

freeStyleJob('NIGHTLY_database-service') {
  description('Kicks off SNAPSHOT Pipeline for the database-service project. Polls for scm changes nightly 03:30-03:45.')
    logRotator(-1, 10, -1, -1)
   
    scm {
        git {
                    remote {
                       url('https://gitlab.com/thetezz/database-service.git')
                    }
                    branch('*/development')
                    extensions{
                      wipeOutWorkspace()
                    }
                  
                }
      
    }
  triggers{
    cron('H(30-45) 3 * * *')
    
  }
  

    steps {
        downstreamParameterized {
            trigger('Snapshot_Pipeline') {
                parameters {
                    predefinedProps([PRODUCT_NAME: 'database-service', BRANCH_NAME: 'development'])
                }
            }

        }
    }

    
}

pipelineJob('Release_Pipeline') {
  logRotator(-1, 10, -1, -1)
  parameters{
    choiceParam('PRODUCT_NAME', ['database-service', 'cas-service', 'StormProject'],
                'artifact ID of the RPM to build' )
    stringParam('BRANCH_NAME', "development", 'branch to build')
    stringParam('NEW_PRODUCT_VERSION', null, 'If blank, the product build number will increment.\nexample, "1.2.3.4" will become "1.2.3.5"')
  }
  
  definition {
        cpsScm {
            scm {
                git {
                    remote {

                        url('https://gitlab.com/thetezz/release-pipeline.git')
                    }
                    branch('*/master')
                    extensions{
                      wipeOutWorkspace()
                    }
                  
                }
            }
        }
    }
}



freeStyleJob('Install_pipeline-utils') {
    logRotator(-1, 1, -1, -1)
   
    scm {
        git {
                remote {
                    url('https://gitlab.com/thetezz/pipeline-util-maven-plugin.git')
                }
                branch('*/master')
                extensions{
                    wipeOutWorkspace()
                }
                  
            }
    }
    steps {
      shell('/var/maven_home/apache-maven-3.5.4/bin/mvn install')
    }  
}


freeStyleJob('Install_version') {
    logRotator(-1, 1, -1, -1)
   
    scm {
        git {
                remote {
                    url('https://gitlab.com/thetezz/version.git')
                }
                branch('*/master')
                extensions{
                    wipeOutWorkspace()
                }
                  
            }
    }
    steps {
      shell('/var/maven_home/apache-maven-3.5.4/bin/mvn install')
    }  
}
